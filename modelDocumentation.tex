\documentclass[10pt,a4paper]{article}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\author{Peeter Meos}
\title{Multispectral Bitmap Merge Documentation}
\begin{document}

\maketitle
\tableofcontents

\section{Introduction}
The purpose of this paper is to provide some background information and documentation for Proekspert Data Science recruitment assignment. The task was to write a short code that merges grayscale images taken in different frequency bands of visible light into one RGB bitmap. This paper describes the approach that was taken, documents the code and gives instructions on running the code.

\section{Approach}
The idea that was implemented is quite straightforward. Input data is provided as a set of grayscale files of the same size. These files represent multispectral layers. Every multispectral layer needs to be transformed into RGB color space based on the stackoverflow suggestions of spectral mapping. Then all layers that are already in RGB space need to be added. In order to retain the initial scale of (0...1), the images' color values need to be scaled down before adding.

When that is done, the whole image need to be saved as a bitmap.

\section{Implementation}
The implementation code was written in R. The following description walks though the code. 

\subsection{Init}
Before doing the actual processing the following steps are needed:
\begin{itemize}
\item Initalise the configuration variables

\item Get the list of all \texttt{.png} files in the given folder
\begin{verbatim}
fn <- list.files(paste(".", fn.path,  sep="/"), pattern=fn.extension)
\end{verbatim}

\item Initalise master RGB image as a $\mathbb{R}^{3\times n}$ matrix of zeroes.
\begin{verbatim}
img.master <- matrix(0, nrow=3, ncol=n.row * n.col)
\end{verbatim}

\item Create a vector of wavelengths with given beginning wavelength and step. The length is the same as the length of filenames' vector for layers.

\begin{verbatim}
wl <- seq(from=wl.start, by=wl.band, length.out = length(fn))
\end{verbatim}

\end{itemize}

\subsection{Main processing loop}
Done that, we are ready to do the actual image processing by looping through all the grayscale image fails. Therefore, in the main loop we do the following:

\begin{enumerate}

\item Import the next .png layer as a $\mathbb{R}^2$ matrix.
\begin{verbatim}
png.tmp <- readPNG(paste(".", fn.path, fn[i], sep="/"))
\end{verbatim}

\item If necessary, adjust the size of the matrix to ensure that all bitmaps have the same dimension.
\begin{verbatim}
  png.img <- matrix(0, nrow=n.row, ncol=n.col)
  png.img[(1:min(n.row, dim(png.tmp)[1])),(1:min(n.col, dim(png.tmp)[2]))] <- 
  png.tmp[(1:min(n.row, dim(png.tmp)[1])),(1:min(n.col, dim(png.tmp)[2]))]
\end{verbatim}

\item Convert the grayscale matrix into one dimensional vector and transform that $\mathbb{R}^n$ vector into $\mathbb{R}^{3\times n}$ RGB space. Since the helper function is vectorised, we can do the whole operation in one go and avoid using \texttt{sapply}, or heaven forbid, \texttt{for} loops.
\begin{verbatim}
img.tmp <- wavelengthToRGB(c(png.img), wl[i])
\end{verbatim}

\item Add this bitmap to the master bitmap
\begin{verbatim}
img.master <- img.master + img.tmp
\end{verbatim}

\end{enumerate}

\subsection{Final image compilation and postprocessing}
After completing this for all grayscale bitmaps do the following:
\begin{itemize}
\item Scale the master bitmap down to original (0..1) range. Simply done by dividing its values by the count of grayscale bitmaps. At the same time adjust the intensity of the image (saturation) by a given coefficient.
\begin{verbatim}
img.master <- img.master * 1 / length(fn) * intensity
\end{verbatim}

\item Check that all RGB values are within allowable bounds.
\begin{verbatim}
img.master <- pmin(img.master, 1)
img.master <- pmax(img.master, 0)
\end{verbatim}

\item Convert this matrix that is still in $\mathbb{R}^{3\times n}$ where n equals $512 \times 512$ into actual RGB matrix in $\mathbb{R}^2$.
\begin{verbatim}
img.rgb <- rgb(red  = matrix(img.master[1,], nrow=n.row, ncol=n.col),
              green = matrix(img.master[2,], nrow=n.row, ncol=n.col),
              blue  = matrix(img.master[3,], nrow=n.row, ncol=n.col)
              )
dim(img.rgb) <- c(n.row, n.col)
\end{verbatim}

\item Export the RGB matrix as the final \texttt{.png} image.
\begin{verbatim}
cat(paste("Exporting :", fn.out, "\n"))
png(filename=fn.out, width=n.row, height = n.col)
grid.raster(img.rgb, interpolate=FALSE)
dev.off()
\end{verbatim}
\end{itemize}

\subsection{Spectral conversion}
The conversion function \texttt{wavelengthToRGB} is written in file \texttt{func.R}. 
The function takes two arguments: a vector \texttt{value} of grayscale intensity values in the range $0\ldots 1$ and wavelength $wl$ in nanometers. Both arguments are required. Vector can be of arbitrary size.


The function returns an RGB matrix with rows respectively for red, green and blue values.
The code snippet itself is ported from a respective wiki available at the this URL\footnote{\url{http://www.noah.org/wiki/Wavelength_to_RGB_in_Python}}. The following Stackoverflow discussion was also helpful \footnote{\url{http://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum}}.
 
The function essentially separates the visible spectrum between 380 and 780 nanometers into 6 separate bands and within those bands uses linear interpolation to calculate the respective RGB values based on the actual wavelength given.


After having calculated the RGB values the given grayscale intensity vector of length $n$ is multiplied by those values resulting a $3\times n$ matrix of RGB values that is returned as a result.

\section{Running the code}
The code is written in R version 3.3.2. Since there is nothing fancy in the code, it should also run on earlier version. The following packages and files are required for the operation:

\begin{itemize}
\item \emph{png} - Required for importing the grayscale png images. Available at \url{https://cran.r-project.org/web/packages/png/index.html}
\item \emph{grid} - Required for exporting the RGB matrix as a bitmap image. From version 1.8.0 onwards base package that is available with standard R installation.
\item \texttt{"func.R"} - This file contains code for converting grayscale image at given wavelength into its RGB equivalent. File is provided with the rest of the source.
\end{itemize}

Other than that, in order to run the code, save it file \texttt{multispectral.R} to the same working folder with the folder that contains multispectral images. The run R and give it a go by sourcing it: \texttt{source("multispectral.R")}. That will run the code and produce the RGB bitmap file \texttt{sponges\_RGB.png} and put it to the same working directory.  

\subsection{Configuration}
If the folder names, bitmap sizes etc have changed, then feel free to change the configuration parameters present in the beginning of the main code file \texttt{multispectral.R}. It is possible to adjust the following values:

\begin{itemize}
\item File IO parameters or input and output
\begin{verbatim}
fn.path      <- "multispectral_images"
fn.extension <- "*.png"
fn.out       <- "sponges_RGB.png"
\end{verbatim}

\item Wavelength parameters (nanometers)
\begin{verbatim}
wl.start <- 400
wl.band  <- 10
\end{verbatim}

\item Image dimensions in pixels - we assume the standard dimensions
\begin{verbatim}
n.row <- 512
n.col <- 512
\end{verbatim}

\item Colour saturation coefficient
\begin{verbatim}
intensity <- 3.8
\end{verbatim}

\end{itemize}


\section{Results}
Current resulting RGB image is displayed below:

\begin{figure}[h]
\includegraphics[scale=0.5]{sponges_RGB.png} 
\caption{Merged RGB image}
\end{figure}

\end{document}

