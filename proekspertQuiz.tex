\documentclass[10pt,a4paper]{article}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\author{Peeter Meos}
\title{Proekspert Data Science Quiz}

\begin{document}
\maketitle

\section{Question 1}
\emph{Describe the preprocessing steps you would undertake to put the data into a form suitable for modelling.}

From here onwards I will be describing my approaches based on R. I would assume that the credit quality ratings data is stored in somewhere in a database (usually some flavour of SQL). In a worst case the modelling has been done in Excel and I have to work with \emph{.xlsx} or \emph{.csv} files. At any rate the first step would be to import the data into R data frames. In case the 10000x52 ratings variables and the feature are on separate tables, the data frames need to be merged. That would require that the institution provides an additional field, an individual ID, to do the join on. 

Given that the raw data is imported, the next step is to clean the dataset. That is to ensure that all the input variables are of the required type and within sensible range. That means that we don't have any NA's, empty values or otherwise nonsensical data. Also, if there are any text and datetime variables, then these encodings usually don't work at the first try and those imports usually need to be tinkered with.

Lastly, if necessary I would do rescaling, if required. When that is done, the dataset should be ready for modelling. 

\section{Question 2}
\emph{Describe in great detail the steps you would undertake in data partitioning to ensure low generalization error of your model.}

I suppose the question is asking how to partition the data in order to obtain good out of sample fit for the model. There are few options for partitioning: 

\begin{itemize}
\item Hold out method - split data into training and validation set. Pick the best performing model based on validation set.
\item Cross-validation - either K-fold cross validation, where I divide the data into training and validation data sets in K different orthogonal chunks or Leave-One-Out cross validation, where across $n$ runs I validate only on 1 sample per run and use the rest for training.
\item As another variation of the same theme, use random subsampling to generate required $K$ training and validation sets. Essentially it is the same as bootstrapping method used in statistics and econometrics to draw inferences on the data sets.
\end{itemize}

The first option has a major drawback due to possibly small sample sizes and uncontrolled biases in selecting just one training and validation set. However, in extremely large sets that can be randomized (ie. we are not requiring autocorrelation in time series etc) it can be efficient. However, cross-validation, where you re-sample the data should yield more stable and robust results in the expense of computational time required to fit $n$ models. 

Therefore I would first build $K$ training and validation sets from our input data. Since the data is not sequential, I would use random sampling for training set generation. I would use roughly 2/3 of data for training and 1/3 for validation. The dataset is not too big, so the generic rule of doing 10 folds should be enough.

After, based on the folds, the best model has been selected, I would generate a fresh test set from the whole dataset and test the performance of the model that had been aggregated across all folds. The parameters will not be tuned, based on the test results. 

\section{Question 3}
\emph{Explain intuitively (but in detail) Curse of Dimensionality and the process of PCA.}

\begin{enumerate}
\item Curse of Dimensionality is essentially the same as exponentiality. That is, linear increase in dimensions (for instance trivial increase from 10 to 15) variables, increases problem's complexity in exponential fashion, possibly rendering it unsolvable in all available time. It is not a big issue with regression models, but can become a problem with neural nets (especially multi-layered specimens). Dimensionality issues are extremely intense in the fields of dynamic programming and mixed integer programming. In the first case only simple dynamic programs are classically solvable, in the second case multiple modeling and decomposition tricks need to be employed to push the dimensionality curse one step further. Eliminating it is usually impossible. The key is the trade-off between the computer time and the resolution of the model. 

\item The point of PCA is dimensionality reduction. In the dataset under analysis lives in a space of 52 dimensions. Usually the data across some dimensions is correlated, that is strong multicollinearity exists. The predictive models (linear regression especially) perform remarkably poorly while giving deceptively good results ($R^2$ values) when strong multicollinearity is present. So to get rid of that we use PCA.

Intuitively our goal is to pick a new coordinate system that has fewer dimensions and doesn't come with a too significant loss of information compared to the original. We do that as follows. In $\mathbb{R}^n$ space we choose a vector $v$ in such a way that the distances between the data points and the vector are minimised. Think of it as similar to linear regression. In actual PCA we then turn it into an unit vector. Then we subtract that vector from the original data and repeat the process for pretty much as long the residuals of the data are converging. As a result we get a lets say new $m$ dimensional vector space where $m < n$. Then with a linear transform we can convert our data from $\mathbb{R}^n$ to $\mathbb{R}^m$ thus reducing the effects of multicollinearity and making the dataset more compact.

The weak point about PCA is the intuitiveness of the results. While the original dimensions have strong relationship to the real life problem, then the new dimensionality reduced coordinate system is abstract in nature and cannot usually easily explained (especially true when briefing the echelons of higher management!).
\end{enumerate}

\section{Question 4}
\emph{How do you proceed with missing data?}

First, hopefully we can find the missing data! I would spend some significant effort not to replicate other's work. If that doesn't work then are there additional information sources that can tell us if the actual historical default has happened? First, I'd try to recover as much as possible.

If the data indeed cannot be recovered within sensible time frame I would try to build a model to second guess the default measurement. Assuming that I have a permission to do that I would move from simple to complex starting with \texttt{logit} and \texttt{probit} models. If I still have time I'd probably investigate neural nets using the subset with complete data as a training set. In order to verify and validate I'd run the model through the actual credit experts who can contribute to the mathematical metods with their experience.


\section{Question 5}
\emph{Describe the effects of using linear regression}

\begin{enumerate}
\item  Linear regression for explaining data in space $\mathbb{N} \in \{0,1\}$ is poor because the endogenous variable is non-linear. Thus there's nothing to stop the model giving values that are either less than zero or greater than one. Even if it would be possible to bound the regression to 0-1 range, the resulting values would cover the whole range more or less evenly or in a worst case, lie close to 0.5. Therefore the response curve between 0 and 1 needs to be non-linear. Either in a shape of sigmoid function or a CDF of a Gaussian distribution. The ideal would be a step function, but we still need to stay in a domain of continuous functions.

\item Using MSE (aka the usual variance) is in that case not preferred because the fit is symmetrical. The model's residuals are minimised in a similar fashion regardless of the sign. In a linear regression model with a decent fit the residuals are normally distributed and the normal distribution is symmetric around the mean (in this case zero).  In our case we want non-symmetric residuals, ie that the model should prefer lower credit scores, thus the residuals should be skewed to the right.
\end{enumerate}

\section{Question 6}
\emph{Describe expectations and the effects of different cost functions}

\begin{enumerate}
\item The unconditional expectation would give the generic expected default rating  for an individual, if no past information was known. The conditional expectation would provide the default rating given the past credit scores of the individual.
\item Using MAD (ie absolute difference) would be preferred when resulting ratings are strongly biased towards either side. In our case it is most likely that most of the default ratings are zero with some 1's as ourliers on far right. Therefore using arithmetic mean would skew the results to the right and using median as a descriptive statistic would be more appropriate.
\end{enumerate}

\section{Question 7}
\emph{Describe effects of logistic regression}
\begin{enumerate}
\item MLE for parameter estimation goes as follows. First you formulate the likelihood function, written as following product:

\begin{equation}
  L(x, \theta) = \prod f(x, \theta)
\end{equation}

Then the MLE estimate of the parameter $\theta$ is the one that minimises the above function. Therefore, taking the first derivate, setting it equal to zero and solving the equation for $\theta$ gives the max likelihood estimate for the parameter. In order to make the algebra simpler, it is often useful to take natural logarithm of the both sides of the equation. Log is a monotonous function that doesn't screw up the maximisation results but allows us to sum instead of multiplying, which is always preferred.

\item Unfortunately for logistic regression, there is no closed form analytical solution for the parameter vector $\beta$. Instead of that, iterative workarounds such as Newton's method can be used. The idea is picking a starting point and then iterating on the solution surface towards the steepest ascent until the solution converges to the hopefully global maximum.

\item The feature effects are described by the elements of $\hat{\beta}$ vector. In essence the magnitude of $i$th element in $\hat{\beta}$ gives the relative increase of probability default per unit increase of $i$th parameter in input vector $X$.

\item The significance of logistic regression estimates is tested with Wald's test.  The idea of Wald's test is that dividing the parameter estimate by its standard error yields values that follow standard gaussian distribution. It's usually provided by standard modeling routines such as \texttt{glm} in R. The usual applies, pick a significance level and compare P-values.

\end{enumerate}
\section{Question 8}
\emph{Describe the employment of neural networks}

\begin{enumerate}
\item Since neural networks are essentially black boxes, classical non-linear optimisation methods for parameters tend not to be usable. Instead, heuristics are used (simulated annealing, genetic optimisation, Tabu search etc). I will briefly describe genetic optimisation. Essentially genetic optimisation first generates a population of models with random parameters. Then on the training set the goodness of fit (via an objective function) is calculated. The best fitting parameter sets survive. For the next generation, the surviving parameter sets mutate (ie. introduce fresh entropy to the system) and breed (ie combine their "genes"). Iterating this way does not guarantee the global optimal, but converges to sufficiently close values.

\item Added layers of DNN increase the ability of the neural network to detect additional features. However, every added degree of freedom (ie. parameter) increases the risk of overfitting the model. In combination with the exponential computation time, the deep neural networks need to be employed in careful and specific settings and are by no means a good solution for any an all feature selection problems.

\item Overfitting is usually reduced by reducing the degrees of freedom. It is essentially the same with all the statistical tools, be it a deep neural network or linear regression model. The general answer to the question is to make the model as simple as possible. The lure of any additional statistically significant parameters should be avoided. In addition the training set (or in that sense the whole input data) should be as orthogonal as possible in order to avoid multicollinearities and unnecessary dimensions.  

\end{enumerate}

\section{Question 9}
\emph{Explain the threshold selection for classification models}

The threshold selection is  done via analysing the receiver operating characteristic (ROC) curve or calculating the sensitivity index (via true and false positives and negatives). However there is no automatic method to calibrate the thresholds since there is always a tradeoff between false positives and false negatives. The threshold is best selected iteratively by involving SMEs (Subject Matter Experts). Otherwise there is a risk of running into credibility issues with decision makers (often called \emph{military judgement} in my previous life).

\section{Question 10}
\emph{Explain model quality control}

\begin{enumerate}
\item Model evaluation metrics to be used depend on the modeling method used. General modelling practices prescribe receiver operating characteristic curves, confusion matrices, K-S charts, F tests, $R^2$ values and AIC criteria to evaluate the models. In addition I have found that using SMEs (Subject Matter Experts) is always helpful to provide the essential sanity check to the model.

\item I would most likely start with the ROC curves. 
\end{enumerate}
 

\section{Question 11}
\emph{Explain why using models in ensemble helps}

Combining different models together (for instance SVM, neural nets, logistic regression, etc) adds robustness to the overall solution. While all models yield bias, in a good ensemble models' biases cancel indirectly each other out. So by using separate models and picking the best common fit ensures that lower generalisation errors, better goodness of fit and reduces chances of producing erroneous results due to overfitting.

\section{Question 12}
\emph{Describe solutions for big datasets}

I suppose the question refers to RAM. If the dataset is too big to fit into memory of any single machine then I would explore the non-exhaustive list of options below:

\begin{itemize}
\item Sample the data. There's always an option to operate on the representative subsets of data. Since the feature space of 52 variable is not that big, then using all 100M customers is not necessary. That is the added value of using all customer data is low. Instead of the whole population, pick representative samples.
\item If still, analysis of the whole population is needed, then the fitting can be always parallelised across many machines. R is not ideal for that work (although Revolution R is better than stock CRAN R at this), so possibly the model needs to be rewritten in Java or Python.
\item If all else fails, then there are Big Data solutions like Hadoop and Spark. However, the problem setup is simple enough in my opinion not to justify hardcore Big Data solutions. Sampling should be good enough.  
\end{itemize}

\section{Question 13}
\emph{Feedback over the quiz}

The quiz was:
\begin{enumerate}
\item Interesting - 10 points for both the quiz and the assignment - you are asking the right questions and playing with RGB bitmaps was something new.
\item Fun - 5 points for quiz - So-so, definitely tickled my brain in a right way! 10 points for the assignment - definitely enjoyed playing with the bitmaps.
\item Difficult - 3 points for the quiz and 5 for the assignment - I'm a bit rusty on Neural Networks, but otherwise it was rather easy. The assignment was actually not that difficult. The interesting and difficult bit was to get the conversion from intensity and wavelength into RGB done in accurate and elegant way.
\end{enumerate}
\end{document}