# README #

This repo contains the responses to Proekspert data science recruitment quiz+assignment.

### Contents ###

* Responses to Data Science Quiz (PDF with LaTeX source):
  proEkspertQuiz.tex
  proEkspertQuiz.pdf
* Implementation of multispectral image merge assignment:
  multispectral.R
  func.R
* Documentation for the assignment above (PDF with LaTeX source):
  modelDocumentation.tex
  modelDocumentation.pdf

### Setup instructions ###

* Summary of set up
  Simply fetch the repo to a local working folder and the code should be ready to go.
  
* Configuration
  No additional configuration should be necessary. For any tweaking, see the model
  documentation, or better yet, look at the configuration section in multispectral.R
  
  The whole setup should be OS agnostic. Albeit it's written on Windows machine, it
  should work fine on Linux and MacOS.
  
* Dependencies
  R source depends on two packages: png and grid. Package png is not a part of base 
  installation, so if you don't have it already, then install.packages("png") should help.
  Package "grid" comes with base R.
  As far as R goes, there are no additional dependencies. 
  If you wish to recompile the documentation and quiz from LaTeX sources, then the only
  non-base package that it depends on is "hyperref", everything else such as babel, amsmath 
  etc should in in base installation of your LaTeX engine.
  
### Contact details ###

* Repo owner or admin
  Peeter Meos <peeter@sigmaresearch.eu>
